//
//  ConsultaCiudadViewController.swift
//  AppClima
//
//  Created by Laboratorio FIS on 31/10/17.
//  Copyright © 2017 DAVID. All rights reserved.
//

import UIKit

class ConsultaCiudadViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBOutlet weak var ciudadTextField: UITextField!
    
    
    @IBAction func consultarButton(_ sender: Any) {
        let service = File()
        service.ConsultaCiudad(city: ciudadTextField.text!){ (weather) in
            
            DispatchQueue.main.async {
                self.climaLable.text = weather
                
            }
        }
    }
    
    @IBOutlet weak var climaLable: UILabel!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
