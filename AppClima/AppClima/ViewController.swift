//
//  ViewController.swift
//  AppClima
//
//  Created by Laboratorio FIS on 25/10/17.
//  Copyright © 2017 DAVID. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
      
    
    }
    
  //MARK:- Outlets
    
    
    @IBOutlet weak var usuarioTextField: UITextField!
    
    @IBOutlet weak var contraseñaTextField: UITextField!
    override func viewWillDisappear(_ animated: Bool) {
        usuarioTextField.text = ""
        contraseñaTextField.text = ""
        usuarioTextField.becomeFirstResponder()
    }
    
    //MARK:- Actions
    
    @IBAction func entrarButton(_ sender: Any) {
        let user = usuarioTextField.text ?? ""
        let contraseña = contraseñaTextField.text ?? ""
        switch(user , contraseña){
        case ("david" , "david"):
            performSegue(withIdentifier: "ciudadSegue", sender: self)
            print ("login correcto")
        case ("david" , _):
            mostrarAlerta(mensaje: "contraseña incorrecta")
           
        default:
      
           mostrarAlerta(mensaje: "credenciales incorrectas")
        }
        
    }
    
    private func mostrarAlerta(mensaje:String){
        let alertView = UIAlertController (title: "Error", message: mensaje, preferredStyle: .alert)
        let aceptar = UIAlertAction(title: "Aceptar", style: .default){
            (action) in
            self.usuarioTextField.text = ""
            self.contraseñaTextField.text = ""
        }
        alertView.addAction(aceptar)
        present(alertView, animated: true, completion: nil)
    }
    
    @IBAction func invitadoButton(_ sender: Any) {
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

