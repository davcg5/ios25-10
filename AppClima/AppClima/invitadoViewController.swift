//
//  invitadoViewController.swift
//  AppClima
//
//  Created by Laboratorio FIS on 25/10/17.
//  Copyright © 2017 DAVID. All rights reserved.
//

import UIKit
import CoreLocation
class invitadoViewController: UIViewController, CLLocationManagerDelegate {
let locationManager = CLLocationManager()
    var bandera = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
       
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled(){
            locationManager.delegate = self
            locationManager.startUpdatingLocation()
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Location Manager Delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = manager.location?.coordinate
        if !bandera{
            consultarPorUbicación(
                lat:(location?.latitude)!,
                lon:(location?.longitude)!
            )
            bandera = true
        }
    }
    
    

    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var weatherLabel: UILabel!
    private func consultarPorUbicación (lat:Double, lon:Double) {
        let service = File()
        service.invitado(lat: lat, lon: lon){(weather, ciudad) in
            DispatchQueue.main.async {
                self.cityLabel.text = weather
                self.weatherLabel.text = ciudad
            }
            
        }
    }

    
    @IBAction func salirButton(_ sender: Any) {
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
/*
     import UIKit
     import CoreLocation
     
     class InvitadoViewController: UIViewController, CLLocationManagerDelegate {
     
     @IBOutlet weak var weatherLabel: UILabel!
     let locationManager = CLLocationManager()
     var bandera = false
     
     //MARK:- ViewController LifeCycle
     override func viewDidLoad() {
     super.viewDidLoad()
     
     locationManager.requestWhenInUseAuthorization()
     
     if CLLocationManager.locationServicesEnabled() {
     
     locationManager.delegate = self
     locationManager.startUpdatingLocation()
     
     }
     
     //consultarPorUbicación()
     }
     
     //MARK:- LocationManager Delegate
     
     func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
     let location = manager.location?.coordinate
     if !bandera{
     consultarPorUbicación(
     lat:(location?.latitude)!,
     lon:(location?.longitude)!
     )
     bandera = true
     }
     }
     
     
     //MARK:- Actions
     private func consultarPorUbicación (lat:Double, lon:Double) {
     let urlStr = "http://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=f4b14259b70ff9328f483a7729e0c980"
     
     let url = URL(string: urlStr)
     let request = URLRequest(url: url!)
     
     let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
     
     if let _ = error {
     return
     }
     
     do {
     
     let weatherJson = try JSONSerialization.jsonObject(with: data!, options: [])
     let weatherDict = weatherJson as! NSDictionary
     guard let weatherKey = weatherDict["weather"] as? NSArray else {
     DispatchQueue.main.async {
     self.weatherLabel.text = "Ciudad no valida"
     }
     return
     }
     let weather = weatherKey[0] as! NSDictionary
     DispatchQueue.main.async {
     self.weatherLabel.text = "\(weather["description"] ?? "Error")"
     }
     } catch {
     print("Error al generar el Json")
     }
     }
     task.resume()
     }
     
     }
     */
}
