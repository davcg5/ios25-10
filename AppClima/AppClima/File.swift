//
//  File.swift
//  AppClima
//
//  Created by Laboratorio FIS on 7/11/17.
//  Copyright © 2017 DAVID. All rights reserved.
//

import Foundation
class File {
    func ConsultaCiudad(city:String, completion:@escaping (String) -> ()){
        let urlStr = "http://api.openweathermap.org/data/2.5/weather?q=\(city)&appid=367ed9b4eeb27bafedd8ca6c97bb83c7"
            consultarButton(urlStr: urlStr) { (weather, ciudad) in
            completion (weather)
        }
    
    }
    func invitado(lat:Double, lon:Double, completion :@escaping (String, String) -> ()){
         let urlStr = "http://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=f4b14259b70ff9328f483a7729e0c980"
            consultarButton(urlStr: urlStr) { (weather, ciudad) in
            completion (weather, ciudad)
        
        //  consultarButton(urlStr: urlStr)
    }
    }
    func consultarButton(urlStr:String, completion:@escaping (String, String)->()) {
       
        
        let url = URL(string:urlStr)
        let request = URLRequest(url: url!)
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            //  print("error: \(error)")
            //print("response: \(response)")
            //print("data: \(data)")
            if let _ = error {
                return
            }
            
            do{
                let weatherJson = try JSONSerialization.jsonObject(with: data!, options: [])
                let weatherDict = weatherJson as! NSDictionary
                guard let weatherKey = weatherDict["weather"] as? NSArray else {
                    DispatchQueue.main.async {
                    completion ("ciudad no válida", "nill")
                    return
                    }
                    return
                    
                }
                let weather = weatherKey[0] as! NSDictionary
                
                DispatchQueue.main.async {
                   completion("\(weather["description"] ?? "Error")" , "\(weatherDict["ciudad"] ?? "Error")")
                }
                
                print (weatherKey)
                print("-----")
                
                print(type(of: weatherJson))
            } catch{
                print("Error al generar el Json")
            }
            
        }
        task.resume()
    }
}

